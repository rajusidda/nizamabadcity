<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Selected Product</title>
<script language="JavaScript">
	function IsBlank(s) {
		var len = s.length;
		for (i = 0; i < len; i++) {
			if (s.charAt(i) != " ") {
				return false;
			}
		}
		return true;
	}
	function Validate_Comment() {
		var str = document.comment.comment.value;
		if (IsBlank(str)) {
			alert("Please Enter Comment")
			return false;
		}
		return true;
	}
	function processForm() {
		if (!Validate_Comment(document.comment.comment.value))
			return false;
	}
</script>
</head>
<body bgcolor="#0489B1">
	<form action="CommentServlet" method="post" name="comment">
		<center>
			<h1>
				<font color="#B9E272" size="20" face="Algerian">Know Your
					Gadget</font>
			</h1>
			<p>
				<font color="#FFFFF" size="6"
					face="Lucida Calligraphy,Comic Sans MS,Lucida Console"> The
					Place where you can judge right place for purchasing right product
				</font>
			</p>
		</center>
		<%
			String pid = request.getParameter("Id");
			Long userId = (Long) request.getSession().getAttribute("userId");
			/* String productId = session.getAttribute("productID"); */
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/kyg", "root", "root");
				System.out.println("ok1");

				PreparedStatement ps = conn.prepareStatement("select * from product where pid = '" + pid + "'");
				ResultSet rs = ps.executeQuery();
				System.out.println("ok2");
				while (rs.next()) {
					System.out.println("ok3");
		%>
		<input type="hidden" value="<%=userId%>" name="userId">
		<table border="1" align="center">
			<tr>
				<td colspan="2"><img src="<%=rs.getString("image")%>"
					height="150" width="300"></a></td>
			</tr>
			<br>
			<br>
			<br>
			<tr>
				<td bgcolor="#FFFFF">Product Id</td>
				<td bgcolor="#FFFFF"><input type="text"
					value=<%=rs.getString("pid")%> name="pid" class="field left"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFF">Product Name</td>
				<td bgcolor="#FFFFF"><%=rs.getString("pname")%></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFF">Online Store</td>
				<td bgcolor="#FFFFF"><%=rs.getString("online")%></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFF">Rating</td>
				<td bgcolor="#FFFFF"><%=rs.getString("rate")%></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFF">Price</td>
				<td bgcolor="#FFFFF"><%=rs.getString("price")%></td>
			</tr>

		</table>
		<table align="center">
			<tr>
				<td>Comment:</td>
				<td><textarea name="comment" rows="4" cols="20"
						ONCHANGE="Validate_Comment()";></textarea><input type="submit"
					value="submit" ONCLICK="return processForm()";></td>
			</tr>
		</table>
		<br>
		<%
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
		%>
		<center>
			<a onclick="history.back(-1)"> Back </a>&nbsp;&nbsp;&nbsp;&nbsp; <a
				href="index.jsp"> Home</a>
		</center>
	</form>
</body>
</html>