package com.geek.pojo;

import javax.persistence.Entity;

@Entity
public class DepartmentPojo {
	private int id;
	private String code;
	private String fromtime;
	private String totime;
	
	public int getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getFromtime() {
		return fromtime;
	}
	public void setFromtime(String fromtime) {
		this.fromtime = fromtime;
	}
	public String getTotime() {
		return totime;
	}
	public void setTotime(String totime) {
		this.totime = totime;
	}

}
