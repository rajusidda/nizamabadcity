package com.geek.global;

import com.geek.pojo.User;

public class SigninStatus {

	public static boolean isSignedIn(User user) {
		String signInStatus = user.getSignInStatus();

		if (signInStatus.equals("true")) {
			return true;
		} else {
			return false;
		}

	}
}
