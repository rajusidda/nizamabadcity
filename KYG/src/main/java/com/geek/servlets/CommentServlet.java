package com.geek.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.PreparedStatement;

public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PreparedStatement st;
		Connection conn;
		
		String pid = request.getParameter("pid");
		String userId = request.getParameter("userId");
		String comment = request.getParameter("comment");
		String status = request.getParameter("praposal");
		System.out.println(pid);
		System.out.println(comment);
		
		try {
		Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/kyg", "root", "root");
			st = (PreparedStatement) conn.prepareStatement("insert into comments values(?,?,?,?)");
			st.setString(1,pid);
			st.setString(2, comment);
			st.setString(3, status);
			st.setString(4, userId);
			st.executeUpdate();
			
			
		}  catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect("./SuccessComment.html");
	}

}
