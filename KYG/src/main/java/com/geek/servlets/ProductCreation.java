package com.geek.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.geek.model.Product_Model;

public class ProductCreation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String pid = req.getParameter("pid");
		String pname = req.getParameter("pname");
		String online = req.getParameter("online");
		String rate = req.getParameter("rate");
		String price = req.getParameter("price");
		String image = req.getParameter("image");
		String flipkart = req.getParameter("flipkart");
		String snapdeal = req.getParameter("snapdeal");
		String zovi = req.getParameter("zovi");
		String category = req.getParameter("category");
		
		Product_Model.saveClass(pid,pname,online,rate,price,image,flipkart,snapdeal,zovi,category);
		resp.sendRedirect("./ProductStored.html");
	}
}
