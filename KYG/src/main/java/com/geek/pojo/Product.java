package com.geek.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Long id;
	private String pid;
	private String pname;
	private String rate;
	private String price;
	private String image;
	private String flipkart_price;
	private String snapdealPrice;
	private String zoviPrice;
	private String catagory;
	
	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}
	public String getCatagory() {
		return catagory;
	}
	
	public String getflipkart_price() {
		return flipkart_price;
	}

	public void setflipkart_price(String flipkart_price) {
		this.flipkart_price = flipkart_price;
	}

	public String getsnapdealPrice() {
		return snapdealPrice;
	}

	public void setsnapdealPrice(String snapdealPrice) {
		this.snapdealPrice = snapdealPrice;
	}

	public String getzoviPrice() {
		return zoviPrice;
	}

	public void setzoviPrice(String zoviPrice) {
		this.zoviPrice = zoviPrice;
	}

	public Long getId() {
		return id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
