package com.geek.model;

import javax.persistence.EntityManager;

import com.geek.pojo.Product;
import com.geek.servlets.EntityManagerProvider;

public class Product_Model {
	public static void saveClass(String pid,String pname,String online,String rate,String price,String image, String flipkart, String snapdeal, String zovi, String category){
		// Here we need entity manager
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();
		
		Product pro = new Product();
		pro.setPid(pid);
		pro.setPname(pname);
		pro.setRate(rate);
		pro.setPrice(price);
		pro.setImage(image);
		pro.setflipkart_price(flipkart);
		pro.setsnapdealPrice(snapdeal);
		pro.setzoviPrice(zovi);
		pro.setCatagory(category);
		
		entityManager.persist(pro);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

}
