package com.geek.model;

import javax.persistence.EntityManager;
import com.geek.pojo.User;
import com.geek.servlets.EntityManagerProvider;

public class User_Model {
	public static void saveClass(String name, String uname, String password, String city, boolean signInStatus) {
		// Here we need entity manager
		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();

		User user = new User();
		user.setName(name);
		user.setUname(uname);
		user.setPassword(password);
		user.setCity(city);
		user.setSignInStatus("true");

		entityManager.persist(user);
		entityManager.getTransaction().commit();
		entityManager.close();
	}
}
