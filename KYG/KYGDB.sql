/*
SQLyog - Free MySQL GUI v5.01
Host - 5.5.20 : Database - kyg
*********************************************************************
Server version : 5.5.20
*/


create database if not exists `kyg`;

USE `kyg`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `Username` char(20) DEFAULT NULL,
  `Password` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert into `admin` values 
('admin','admin');

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `productID` varchar(20) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `userID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comments` */

insert into `comments` values 
('100','good','accept','2'),
('100','',NULL,'1'),
('101','',NULL,'1'),
('101','',NULL,'1');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `online` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `pname` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `comment` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert into `product` values 
(1,'s1.jpg','flipkart','100','mobile','5000','5.0','good'),
(2,'s3.jpg','ZOVI','101','TV','6000','6.0',NULL),
(7,'Lighthouse.jpg','flipgoods','500','mobil','50000','5',NULL),
(9,'Penguins.jpg','df','55','dfsd','3434','5',NULL),
(10,'Penguins.jpg','df','55','dfsd','3434','5',NULL),
(11,'Koala.jpg','nova','23','car','50000','4.3',NULL),
(12,'Hydrangeas.jpg','bajaj','25','byk','85000','4.3',NULL),
(13,'image.jpg','flipgoods','600','tv','50000','5',NULL),
(14,'','','','','','',NULL),
(15,'image.jpg','zovi','500','tv','5000','5',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `signInStatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert into `user` values 
(1,'nzb','santosh','123','santhu',NULL),
(2,'nzb','shylu','shylu','shylu','true'),
(3,'nzb','ram','456','ram','true');
